# @devatscience/ngx-errors

Error handling for Angular.

## How to

### Run the tests

- Install the dependencies : `npm install`
- Run the test suite : `npm test`

### Publish a new version

- Use `npm version` to create a tagged commit for the new version.
- Push it.

## Library content

- error-handler.service (requires `primeng/components/common/messageservice`)
- error.module
- generic-error.component
- not-found-error.component
- error types
  - types/app.error
  - types/clear-ngx-errors.function
  - types/constraint-violations.class
  - types/http.error
  - types/interfaces
  - types/normalize-error.function
  - types/null.error
  - types/type-guards.function
  - types/unhandled.error
  - types/validation.error
  
## Peer dependencies
    "@angular/common": "^8.2.0",
    "@angular/core": "^8.2.0",
    "@angular/forms": "^8.2.0",
    "@angular/router": "^8.2.0",
    "lodash": "^4.17.15",
    "primeng": "^8.0.2"
