import { ErrorHandler, Injectable } from '@angular/core';
import { MessageService } from 'primeng/components/common/messageservice';

@Injectable()
export class ErrorHandlerService implements ErrorHandler {
  private readonly _console: Console = console;

  public constructor(private readonly messageService: MessageService) {}

  public handleError(error: any): void {
    const errorLogger = this.getErrorLogger(error);
    const original = this.getOriginalError(error);
    const context = this.getDebugContext(error);
    const message = (original || error).message || `${error}`;

    errorLogger(this._console, 'ERROR', message);
    if (original) {
      errorLogger(this._console, 'ORIGINAL ERROR', original);
    }
    if (context) {
      errorLogger(this._console, 'CONTEXT', context);
    }

    this.messageService.add({
      severity: 'error',
      summary: (original || error).constructor.name,
      detail: message,
      life: 15,
      closable: true,
      data: error,
    });
  }

  private getDebugContext(error?: Error): any {
    return error
      ? (error as any).ngDebugContext ||
          this.getDebugContext(this.getOriginalError(error))
      : null;
  }

  private getOriginalError(error: Error): Error {
    let original = error;
    while (original) {
      original = (original as any).ngOriginalError;
    }
    return original;
  }

  private getErrorLogger(
    error: Error
  ): (console: Console, ...values: any[]) => void {
    return (error as any).ngErrorLogger || this.defaultErrorLogger;
  }

  private defaultErrorLogger(console: Console, ...values: any[]) {
    (<any>console.error)(...values);
  }
}
