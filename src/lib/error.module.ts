import { ErrorHandler, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ErrorHandlerService } from './error-handler.service';
import { GenericErrorComponent } from './generic-error.component';
import { NotFoundErrorComponent } from './not-found-error.component';

const routes: Routes = [
  {
    path: 'error/404',
    pathMatch: 'full',
    component: NotFoundErrorComponent,
  },
  { path: 'error/**', component: GenericErrorComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [{ provide: ErrorHandler, useClass: ErrorHandlerService }],
  declarations: [GenericErrorComponent, NotFoundErrorComponent],
})
export class ErrorModule {}
