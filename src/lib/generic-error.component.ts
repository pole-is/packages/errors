import { Component } from '@angular/core';

@Component({
  selector: 'lib-generic-error',
  templateUrl: './generic-error.component.html',
})
export class GenericErrorComponent {}
