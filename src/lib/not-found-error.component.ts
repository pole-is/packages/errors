import { Component } from '@angular/core';

@Component({
  selector: 'lib-not-found-error',
  templateUrl: './not-found-error.component.html',
})
export class NotFoundErrorComponent {}
