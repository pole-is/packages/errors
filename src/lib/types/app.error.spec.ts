import { AppError } from './app.error';

describe('AppError', () => {
  let baseErr: Error;
  let err: AppError<Error>;

  beforeEach(() => {
    baseErr = new Error('Bam !');
    err = new AppError(baseErr, baseErr.message, baseErr.stack, false);
  });

  it('.isNullError should be false', () => {
    expect(err.isNullError).toBe(false);
  });

  describe('.dispatch()', () => {
    it('should set error message', () => {
      const handler = jasmine.createSpyObj('handler', ['setError']);
      handler.setError.and.returnValue(true);

      expect(err.dispatch(handler)).toBeTruthy();

      expect(handler.setError).toHaveBeenCalledWith('Bam !');
    });

    it('should set validation errors as fallback', () => {
      const handler = jasmine.createSpyObj('handler', ['setErrors']);
      handler.setErrors.and.returnValue(true);

      expect(err.dispatch(handler)).toBeTruthy();

      expect(handler.setErrors).toHaveBeenCalledWith({ message: 'Bam !' });
    });

    it('should log error', () => {
      const handler = jasmine.createSpyObj('handler', ['logError']);
      handler.logError.and.returnValue(true);

      expect(err.dispatch(handler)).toBeTruthy();

      expect(handler.logError).toHaveBeenCalledWith(
        'Bam !',
        'Error',
        baseErr.stack
      );
    });
  });
});
