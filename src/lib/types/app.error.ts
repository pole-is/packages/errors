import { DispatchableError } from './interfaces';
import {
  isErrorLogger,
  isErrorMessageHandler,
  isValidationErrorHandler,
} from './type-guards.function';

/**
 * Erreur "enveloppant" une autre erreur.
 */
export class AppError<T> implements DispatchableError {
  public readonly isNullError = false;

  public constructor(
    public readonly original: T,
    public readonly message: string,
    public readonly stack?: string,
    public readonly transient = false
  ) {}

  public get name(): string {
    return this.getName();
  }

  public dispatch(handler: any): boolean {
    return (
      (isErrorLogger(handler) &&
        handler.logError(this.message, this.name, this.stack)) ||
      (isErrorMessageHandler(handler) && handler.setError(this.message)) ||
      (isValidationErrorHandler(handler) &&
        handler.setErrors({ message: this.message }))
    );
  }

  protected getName(): string {
    return (
      (this.original instanceof Error && this.original.name) ||
      this.constructor.name
    );
  }
}
