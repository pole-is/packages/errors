import { clearErrors } from './clear-errors.function';

describe('clearErrors', () => {
  it('should clear errors using all ErrorHandler methods', () => {
    const handler = jasmine.createSpyObj('handler', [
      'setError',
      'setConstraintViolations',
      'setErrors',
    ]);

    handler.setError.and.returnValue(true);
    handler.setErrors.and.returnValue(true);
    handler.setConstraintViolations.and.returnValue(true);

    expect(clearErrors(handler)).toBeTruthy();

    expect(handler.setError).toHaveBeenCalledWith(null);
    expect(handler.setErrors).toHaveBeenCalledWith(null);
    expect(handler.setConstraintViolations).toHaveBeenCalledWith(null);
  });

  it('should prefer clearErrors', () => {
    const handler = jasmine.createSpyObj('handler', [
      'setError',
      'setConstraintViolations',
      'setErrors',
      'clearErrors',
    ]);

    handler.clearErrors.and.returnValue(true);

    expect(clearErrors(handler)).toBeTruthy();

    expect(handler.clearErrors).toHaveBeenCalled();
    expect(handler.setError).not.toHaveBeenCalled();
    expect(handler.setErrors).not.toHaveBeenCalled();
    expect(handler.setConstraintViolations).not.toHaveBeenCalled();
  });
});
