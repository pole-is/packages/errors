import {
  isConstraintViolationHandler,
  isErrorClearer,
  isErrorMessageHandler,
  isValidationErrorHandler,
} from './type-guards.function';

/**
 * Supprime les erreurs d'un handler.
 */
export function clearErrors(handler: any): boolean {
  if (isErrorClearer(handler) && handler.clearErrors()) {
    return true;
  }

  let handled = false;
  if (isErrorMessageHandler(handler) && handler.setError(null)) {
    handled = true;
  }
  if (isValidationErrorHandler(handler) && handler.setErrors(null)) {
    handled = true;
  }
  if (
    isConstraintViolationHandler(handler) &&
    handler.setConstraintViolations(null)
  ) {
    handled = true;
  }
  return handled;
}
