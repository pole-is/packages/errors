import { HttpErrorResponse } from '@angular/common/http';

import { ConstraintViolations } from './constraint-violations.class';

describe('ConstraintViolations', () => {
  const violations = [
    {
      propertyPath: 'name',
      message: 'This propperty cannot be blank',
    },
  ];

  let err: ConstraintViolations;

  beforeEach(() => {
    const orig = new HttpErrorResponse({
      error: { violations },
      status: 400,
      statusText: 'Constraint violations',
    });
    err = new ConstraintViolations(
      orig,
      orig.statusText,
      orig.error.violations
    );
  });

  it('.isNullError should be false', () => {
    expect(err.isNullError).toBe(false);
  });

  describe('.dispatch()', () => {
    it('should set error message', () => {
      const handler = jasmine.createSpyObj('handler', ['setError']);
      handler.setError.and.returnValue(true);

      expect(err.dispatch(handler)).toBeTruthy();

      expect(handler.setError).toHaveBeenCalledWith('Constraint violations');
    });

    it('should set constraint violations', () => {
      const handler = jasmine.createSpyObj('handler', [
        'setConstraintViolations',
      ]);
      handler.setConstraintViolations.and.returnValue(true);

      expect(err.dispatch(handler)).toBeTruthy();

      expect(handler.setConstraintViolations).toHaveBeenCalledWith(violations);
    });
  });
});
