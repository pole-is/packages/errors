import { HttpErrorResponse } from '@angular/common/http';

import { BadRequestError } from './http.error';
import { ConstraintViolation } from './interfaces';
import { isConstraintViolationHandler } from './type-guards.function';

/**
 * Erreur de validation retournée par api-platform.
 */
export class ConstraintViolations extends BadRequestError {
  public constructor(
    original: HttpErrorResponse,
    message: string,
    public readonly violations: ConstraintViolation[]
  ) {
    super(original, message);
  }

  public dispatch(handler: any): boolean {
    return (
      (isConstraintViolationHandler(handler) &&
        handler.setConstraintViolations(this.violations)) ||
      super.dispatch(handler)
    );
  }

  public static create(
    violations: ConstraintViolation[]
  ): ConstraintViolations {
    return new ConstraintViolations(
      new HttpErrorResponse({
        status: 400,
        statusText: 'Constraint violations',
      }),
      'Constraint violations',
      violations
    );
  }
}
