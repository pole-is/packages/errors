import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import { AppError } from './app.error';

/**
 * Erreur basée sur une réponse HTTP d'erreur.
 */
export class HTTPError extends AppError<HttpErrorResponse> {
  public constructor(original: HttpErrorResponse, message?: string) {
    super(
      original,
      message || original.message || original.statusText,
      undefined,
      original.status >= 500
    );
  }

  public get status(): number {
    return this.original.status;
  }

  public get statusText(): string {
    return this.original.statusText;
  }

  public get headers(): HttpHeaders {
    return this.original.headers;
  }

  public get url(): string {
    return this.original.url;
  }

  public get error(): string {
    return this.original.error;
  }
}

/**
 * Erreur 403: Forbidden
 */
export class AccessDeniedError extends HTTPError {}

/**
 * Erreur 401: Authentication Required
 */
export class AuthenticationError extends HTTPError {}

/**
 * Erreur 400: Bad Request
 */
export class BadRequestError extends HTTPError {}
