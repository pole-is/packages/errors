import { ValidationErrors } from '@angular/forms';

/**
 * Rapport d'erreur de validation
 */
export interface ConstraintViolation {
  readonly propertyPath: string;
  readonly message: string;
}

/**
 * Types de gestionnaires d'erreurs.
 */

export interface ErrorMessageHandler {
  setError(message: string | null): boolean;
}

export interface ConstraintViolationHandler {
  setConstraintViolations(violations: ConstraintViolation[] | null): boolean;
}

export interface ValidationErrorHandler {
  setErrors(errors: ValidationErrors | null): boolean;
}

export interface ErrorLogger {
  logError(
    message: string,
    name: string,
    stack?: string,
    context?: any
  ): boolean;
}

export interface ErrorClearer {
  clearErrors(): boolean;
}

export type ErrorHandler =
  | ErrorMessageHandler
  | ConstraintViolationHandler
  | ValidationErrorHandler
  | ErrorLogger;

export interface DispatchableError extends Error {
  readonly isNullError: boolean;

  dispatch(handler: ErrorHandler): boolean;
}
