import { HttpErrorResponse } from '@angular/common/http';

import { AppError } from './app.error';
import { ConstraintViolations } from './constraint-violations.class';
import {
  AccessDeniedError,
  AuthenticationError,
  BadRequestError,
  HTTPError,
} from './http.error';
import { normalizeError } from './normalize-error.function';

describe('normalizeError', () => {
  it('should use string as message', () => {
    const err = 'Error';
    expect(normalizeError(err)).toEqual(
      new AppError(err, err, undefined, false)
    );
  });

  describe('should return NullError on empty values', () => {
    const falsies = [null, false, {}, [], '', 0];
    for (const falsy of falsies) {
      it(JSON.stringify(falsy), () =>
        expect(normalizeError(falsy).isNullError).toBeTruthy()
      );
    }
  });

  it('should reuse Error properties', () => {
    const err = new Error('BAM!');
    expect(normalizeError(err)).toEqual(
      new AppError(err, err.message, err.stack, false)
    );
  });

  it('should return AppErrors as is', () => {
    const orig = new Error('BAM!');
    const err = new AppError(orig, orig.message);
    expect(normalizeError(err)).toBe(err);
  });

  it('should classify HttpErrorResponse as HTTPError', () => {
    const err = new HttpErrorResponse({
      error: 'BAM!',
      status: 404,
      statusText: 'Not found',
      url: 'http://example.com',
    });
    const appErr = normalizeError(err);
    expect(appErr instanceof HTTPError).toBeTruthy();
    if (appErr instanceof HTTPError) {
      expect(appErr.message).toContain('BAM!');
      expect(appErr.transient).toBeFalsy();
      expect(appErr.original).toBe(err);
    }
  });

  it('should classify 5xx HttpErrorResponse as transient Error', () => {
    const err = new HttpErrorResponse({ status: 500 });
    const appErr = normalizeError(err);
    expect(appErr instanceof HTTPError).toBeTruthy();
    if (appErr instanceof HTTPError) {
      expect(appErr.transient).toBeTruthy();
    }
  });

  it('should classify 401 HttpErrorResponse as AuthenticationError', () => {
    const err = new HttpErrorResponse({ status: 401 });
    const appErr = normalizeError(err);
    expect(appErr instanceof AuthenticationError).toBeTruthy();
  });

  it('should classify 403 HttpErrorResponse as AccessDeniedError', () => {
    const err = new HttpErrorResponse({ status: 403 });
    const appErr = normalizeError(err);
    expect(appErr instanceof AccessDeniedError).toBeTruthy();
  });

  it('should classify 400 HttpErrorResponse as BadRequestError', () => {
    const err = new HttpErrorResponse({ status: 400 });
    const appErr = normalizeError(err);
    expect(appErr instanceof BadRequestError).toBeTruthy();
  });

  it('should classify sepcial 400 HttpErrorResponse as ConstraintViolations', () => {
    const violations = [
      {
        propertyPath: 'properties',
        message:
          'The product must have the minimal properties required ("description", "price")',
      },
    ];
    const err = new HttpErrorResponse({
      error: {
        '@context': '/contexts/ConstraintViolationList',
        '@type': 'ConstraintViolationList',
        'hydra:title': 'An error occurred',
        'hydra:description':
          'properties: The product must have the minimal properties required ("description",' +
          ' "price")',
        violations,
      },
      status: 400,
    });
    const appErr = normalizeError(err);
    expect(appErr instanceof ConstraintViolations).toBeTruthy();
    if (appErr instanceof ConstraintViolations) {
      expect(appErr.violations).toEqual(violations);
    }
  });
});
