import { HttpErrorResponse } from '@angular/common/http';
import * as _ from 'lodash';

import { AppError } from './app.error';
import { ConstraintViolations } from './constraint-violations.class';
import {
  AccessDeniedError,
  AuthenticationError,
  BadRequestError,
  HTTPError,
} from './http.error';
import { DispatchableError } from './interfaces';
import { NullError } from './null.error';

/**
 * Transforme une valeur en erreur.
 *
 * Les informations intéressantes d'HttpErrorResponse et Error sont conservés.
 */
export function normalizeError(
  err: DispatchableError | HttpErrorResponse | Error | any
): DispatchableError {
  if (err instanceof AppError || err === NullError) {
    return err;
  }
  if (err instanceof HttpErrorResponse) {
    return normalizeHttpErrorResponse(err);
  }
  if (err instanceof Error) {
    return new AppError(err, err.message, err.stack, false);
  }
  if (_.isEmpty(err)) {
    return NullError;
  }
  if (typeof err === 'object') {
    return new AppError(
      err,
      extractMessage(err),
      err.stack,
      err.transient || false
    );
  }
  return new AppError(err, extractMessage(err));
}

function normalizeHttpErrorResponse(err: HttpErrorResponse): HTTPError {
  const body: any = 'error' in err ? err.error : {};
  const message = extractMessage(body) || extractMessage(err);
  switch (err.status) {
    case 400:
      if (_.has(body, 'violations') && _.isArray(body.violations)) {
        return new ConstraintViolations(err, message, body.violations);
      }
      return new BadRequestError(err, message);
    case 401:
      return new AuthenticationError(err, message);
    case 403:
      return new AccessDeniedError(err, message);
    default:
      return new HTTPError(err, message);
  }
}

function extractMessage(err: any): string | undefined {
  if (!err) {
    return undefined;
  }
  switch (typeof err) {
    case 'object':
      if ('message' in err) {
        return err.message;
      }
      if ('hydra:description' in err) {
        return err['hydra:description'];
      }
      if ('hydra:title' in err) {
        return err['hydra:title'];
      }
      if ('error' in err) {
        return extractMessage(err.error);
      }
      break;
    case 'string':
    case 'number':
      return `${err}`;
  }
  return undefined;
}
