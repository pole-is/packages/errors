import { NullError } from './null.error';

describe('NullError', () => {
  it('.isNullError should be true', () => {
    expect(NullError.isNullError).toBe(true);
  });

  describe('.dispatch()', () => {
    it('should clear error message', () => {
      const handler = jasmine.createSpyObj('handler', ['setError']);
      handler.setError.and.returnValue(true);

      expect(NullError.dispatch(handler)).toBeTruthy();

      expect(handler.setError).toHaveBeenCalledWith(null);
    });

    it('should clear constraint violations', () => {
      const handler = jasmine.createSpyObj('handler', [
        'setConstraintViolations',
      ]);
      handler.setConstraintViolations.and.returnValue(true);

      expect(NullError.dispatch(handler)).toBeTruthy();

      expect(handler.setConstraintViolations).toHaveBeenCalledWith(null);
    });

    it('should clear validation errors', () => {
      const handler = jasmine.createSpyObj('handler', ['setErrors']);
      handler.setErrors.and.returnValue(true);

      expect(NullError.dispatch(handler)).toBeTruthy();

      expect(handler.setErrors).toHaveBeenCalledWith(null);
    });

    it('should not log error', () => {
      const handler = jasmine.createSpyObj('handler', ['logError']);
      handler.logError.and.returnValue(true);

      expect(NullError.dispatch(handler)).toBeFalsy();

      expect(handler.logError).not.toHaveBeenCalled();
    });
  });
});
