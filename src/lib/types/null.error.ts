import { clearErrors } from './clear-errors.function';
import { DispatchableError, ErrorHandler } from './interfaces';

/**
 * Singleton d'erreur null.
 */
export const NullError: DispatchableError = {
  isNullError: true,
  name: 'NullError',
  message: 'No error',
  dispatch: (handler: ErrorHandler) => {
    return clearErrors(handler);
  },
};
