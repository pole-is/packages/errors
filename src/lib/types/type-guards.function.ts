import * as _ from 'lodash';

import {
  ConstraintViolationHandler,
  DispatchableError,
  ErrorClearer,
  ErrorHandler,
  ErrorLogger,
  ErrorMessageHandler,
  ValidationErrorHandler,
} from './interfaces';

export function isErrorMessageHandler(that: any): that is ErrorMessageHandler {
  return _.hasIn(that, 'setError') && _.isFunction(that.setError);
}

export function isConstraintViolationHandler(
  that: any
): that is ConstraintViolationHandler {
  return (
    _.hasIn(that, 'setConstraintViolations') &&
    _.isFunction(that.setConstraintViolations)
  );
}

export function isValidationErrorHandler(
  that: any
): that is ValidationErrorHandler {
  return _.hasIn(that, 'setErrors') && _.isFunction(that.setErrors);
}

export function isErrorLogger(that: any): that is ErrorLogger {
  return _.hasIn(that, 'logError') && _.isFunction(that.logError);
}

export function isErrorHandler(that: any): that is ErrorHandler {
  return (
    isErrorMessageHandler(that) ||
    isConstraintViolationHandler(that) ||
    isValidationErrorHandler(that) ||
    isErrorLogger(that)
  );
}

export function isErrorClearer(that: any): that is ErrorClearer {
  return _.hasIn(that, 'clearErrors') && _.isFunction(that.clearErrors);
}

export function isDispatchableError(that: any): that is DispatchableError {
  return _.hasIn(that, 'dispatch') && _.isFunction(that.dispatch);
}
