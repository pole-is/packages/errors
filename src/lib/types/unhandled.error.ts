import { AppError } from './app.error';
import { DispatchableError } from './interfaces';

/**
 * Erreur émise lorsqu'une exception ne peut pas être dispatchée.
 */
export class UnhandledError extends AppError<DispatchableError> {
  public constructor(original: DispatchableError) {
    super(
      original,
      `Unhandled error: ${original.message}`,
      original.stack,
      false
    );
  }
}
