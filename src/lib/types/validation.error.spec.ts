import { ValidationError } from './validation.error';

describe('ValidationError', () => {
  const validationErrors = {
    message: 'This field is required',
    required: true,
  };

  let err: ValidationError;

  beforeEach(() => {
    err = new ValidationError(validationErrors);
  });

  it('.isNullError should be false', () => {
    expect(err.isNullError).toBe(false);
  });

  describe('.dispatch()', () => {
    it('should set error message', () => {
      const handler = jasmine.createSpyObj('handler', ['setError']);
      handler.setError.and.returnValue(true);

      expect(err.dispatch(handler)).toBeTruthy();

      expect(handler.setError).toHaveBeenCalledWith('This field is required');
    });

    it('should set validation errors', () => {
      const handler = jasmine.createSpyObj('handler', ['setErrors']);
      handler.setErrors.and.returnValue(true);

      expect(err.dispatch(handler)).toBeTruthy();

      expect(handler.setErrors).toHaveBeenCalledWith(validationErrors);
    });
  });
});
