import { ValidationErrors } from '@angular/forms';

import { AppError } from './app.error';
import { isValidationErrorHandler } from './type-guards.function';

export class ValidationError extends AppError<ValidationErrors> {
  public constructor(public readonly errors: ValidationErrors) {
    super(errors, errors.message || 'Validation error');
  }

  public dispatch(handler: any): boolean {
    return (
      (isValidationErrorHandler(handler) && handler.setErrors(this.errors)) ||
      super.dispatch(handler)
    );
  }
}
